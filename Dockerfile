FROM ubuntu 
RUN apt update && apt install nginx -y
WORKDIR /app 
COPY ./index.html /app
CMD ["service","nginx","start"]
